# Bitazza app trade.

Show trade coin for user. Admin can add/delete coin from backend.
*Please check production branch for lastest code version.

If you want update code in production. Just checkout new branch and merge to production branch. Gitlab will auto deploy (CI/CD) to server.

## Demo

Please check link : http://35.240.174.240

## Admin user

```bash
username: admin
password: 010203
```

## CI/CD

Only production branch.

## Source

Data from https://bitazza.com/ and for api document check here : https://api-doc.bitazza.com/
