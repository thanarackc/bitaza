const httpStatus = require('http-status')
const { User } = require('../models')
const ApiError = require('../utils/ApiError')

const getUserByEmail = async username => {
  return User.findOne({ where: { username } })
}

const createUser = async userBody => {
  if (await getUserByEmail(userBody.username)) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Username already taken.')
  }
  const user = await User.create(userBody)
  return user
}

module.exports = {
  getUserByEmail,
  createUser
}
