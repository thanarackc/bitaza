const { Instruments } = require('../models')
const ApiError = require('../utils/ApiError')

const findAll = async () => {
  const data = await Instruments.findAll()
  return data
}

const insertOrUpdateStatus = async ({ instrumentId, status }) => {
  const intrument = await Instruments.findOne({ where: { instrumentId } })
  if (intrument) {
    await intrument.update({ status })
    return intrument
  } else {
    const intrument = await Instruments.create({ instrumentId, status })
    return intrument
  }
}

module.exports = {
  findAll,
  insertOrUpdateStatus
}
