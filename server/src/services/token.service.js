const jwt = require('jsonwebtoken')
const config = require('../config/config')
const moment = require('moment')

const generateToken = (user, expires, type, secret = config.jwt.secret) => {
  const payload = {
    sub: user,
    iat: moment().unix(),
    exp: expires.unix(),
    type
  }
  return jwt.sign(payload, secret)
}

const generateAuthTokens = async user => {
  const accessTokenExpires = moment().add(
    config.jwt.accessExpirationMinutes,
    'minutes'
  )
  const accessToken = generateToken(
    { id: user.id, role: user.role },
    accessTokenExpires,
    'access'
  )

  return {
    access: {
      token: accessToken,
      expires: accessTokenExpires.toDate()
    }
  }
}

module.exports = {
  generateAuthTokens
}
