const httpStatus = require('http-status')
const ApiError = require('../utils/ApiError')
const userService = require('./user.service')
const bcrypt = require('bcrypt')

const loginUserWithEmailAndPassword = async (username, password) => {
  const user = await userService.getUserByEmail(username)
  if (!user || !(await bcrypt.compareSync(password, user.password))) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Incorrect username or password.')
  }
  return user
}

module.exports = {
  loginUserWithEmailAndPassword
}
