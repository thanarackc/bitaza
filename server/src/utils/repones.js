const httpStatus = require('http-status')

const commonRespone = ({ res, statusCode, errorCode, errorMessage, data }) => {
  const respone = res.status(statusCode || httpStatus.OK).send({
    status: {
      errorCode: errorCode || null,
      errorMessage: errorMessage || null
    },
    data: data
  })
  return respone
}

module.exports = {
  commonRespone
}
