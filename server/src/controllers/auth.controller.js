const catchAsync = require('../utils/catchAsync')
const { commonRespone } = require('../utils/repones')
const tokenService = require('../services/token.service')
const authService = require('../services/auth.service')
const httpStatus = require('http-status')

const authLogin = catchAsync(async (req, res) => {
  try {
    const { username, password } = req.body
    const user = await authService.loginUserWithEmailAndPassword(
      username,
      password
    )
    const tokens = await tokenService.generateAuthTokens(user)
    return commonRespone({
      res,
      data: {
        accessToken: tokens.access.token,
        expire: tokens.access.expires
      }
    })
  } catch (e) {
    return commonRespone({
      res,
      statusCode: httpStatus.BAD_REQUEST,
      errorCode: httpStatus.BAD_REQUEST,
      errorMessage: e.message
    })
  }
})

module.exports = {
  authLogin
}
