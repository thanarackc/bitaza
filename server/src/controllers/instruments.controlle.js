const { User, Instruments } = require('../models')
const catchAsync = require('../utils/catchAsync')
const { commonRespone } = require('../utils/repones')
const instrumentsService = require('../services/intruments.service')
const httpStatus = require('http-status')

const findAll = catchAsync(async (req, res) => {
  try {
    const data = await instrumentsService.findAll()
    return commonRespone({
      res,
      data: {
        result: data
      }
    })
  } catch (e) {
    return commonRespone({
      res,
      statusCode: httpStatus.BAD_REQUEST,
      errorCode: httpStatus.BAD_REQUEST,
      errorMessage: e.message
    })
  }
})

const update = catchAsync(async (req, res) => {
  try {
    const { status, instrumentIds } = req.body
    if (status && instrumentIds) {
      if (typeof instrumentIds === 'object') {
        for (let i = 1; i <= instrumentIds.length; i++) {
          const val = instrumentIds[i - 1]
          await instrumentsService.insertOrUpdateStatus({
            status,
            instrumentId: val
          })
        }
      }
    }
    return commonRespone({
      res,
      data: {
        message: 'Create user success.'
      }
    })
  } catch (e) {
    return commonRespone({
      res,
      statusCode: httpStatus.BAD_REQUEST,
      errorCode: httpStatus.BAD_REQUEST,
      errorMessage: e.message
    })
  }
})

module.exports = {
  update,
  findAll
}
