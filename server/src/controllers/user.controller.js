const { User } = require('../models')
const catchAsync = require('../utils/catchAsync')
const { commonRespone } = require('../utils/repones')
const userService = require('../services/user.service')
const bcrypt = require('bcrypt')
const httpStatus = require('http-status')
const saltRounds = 10

const index = catchAsync(async (req, res) => {
  return commonRespone({
    res,
    data: {}
  })
})

const getAllUser = catchAsync(async (req, res) => {
  const data = await User.findAll({})
  return commonRespone({
    res,
    data: {
      result: data
    }
  })
})

const createUser = catchAsync(async (req, res) => {
  try {
    const password = await bcrypt.hash(req.body.password, saltRounds)
    const data = {
      ...req.body,
      password
    }
    await userService.createUser(data)
    return commonRespone({
      res,
      data: {
        message: 'Create user success.'
      }
    })
  } catch (e) {
    return commonRespone({
      res,
      statusCode: httpStatus.BAD_REQUEST,
      errorCode: httpStatus.BAD_REQUEST,
      errorMessage: e.message
    })
  }
})

module.exports = {
  index,
  getAllUser,
  createUser
}
