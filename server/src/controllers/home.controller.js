const catchAsync = require('../utils/catchAsync')

const index = catchAsync(async (req, res) => {
  return res.send({
    message: 'Hello World!'
  })
})

module.exports = {
  index
}
