const { DataTypes } = require('sequelize')

module.exports = function(sequelize) {
  return sequelize.define(
    'user',
    {
      name: {
        type: DataTypes.STRING
      },
      username: {
        type: DataTypes.STRING
      },
      password: {
        type: DataTypes.STRING
      },
      role: {
        type: DataTypes.ENUM('admin', 'user')
      }
    },
    {}
  )
}
