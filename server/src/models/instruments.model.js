const { DataTypes } = require('sequelize')

module.exports = function(sequelize) {
  return sequelize.define(
    'instruments',
    {
      instrumentId: {
        type: DataTypes.STRING
      },
      status: {
        type: DataTypes.ENUM('active', 'inactive')
      }
    },
    {}
  )
}
