const { Sequelize } = require('sequelize')
const userModel = require('./user.model')
const instrumentsModel = require('./instruments.model')

// setup mysql connection
const sequelize = new Sequelize('bitazza', 'bitazza', '%bitazAa@12345!', {
  host: '206.189.38.110',
  dialect: 'mysql'
})

const User = userModel(sequelize)
const Instruments = instrumentsModel(sequelize)

try {
  sequelize.sync().then(() => {
    console.log('Database & Table created.')
  })
} catch (e) {
  console.log(e)
}

module.exports = {
  User,
  Instruments
}
