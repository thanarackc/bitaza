// const passport = require('passport');
// const httpStatus = require('http-status');
// const ApiError = require('../utils/ApiError');
// const { roleRights } = require('../config/roles');
const jwt = require('jsonwebtoken')
const config = require('../config/config')
const moment = require('moment')
const { commonRespone } = require('../utils/repones')

const auth = () => async (req, res, next) => {
  console.log('Auth say hi.')
  return new Promise((resolve, reject) => {
    // console.log(req.headers.authorization)
    let token = req.headers.authorization
    if (token) {
      token = token.replace('Bearer ', '')
      try {
        jwt.verify(token, config.jwt.secret)
      } catch (e) {
        return commonRespone({
          req,
          res,
          statusCode: 500,
          errorCode: 500,
          errorMessage: 'Token invalide.'
        })
      }
    } else {
      return commonRespone({
        req,
        res,
        statusCode: 500,
        errorCode: 500,
        errorMessage: 'Token not found.'
      })
    }
    resolve()
  })
    .then(() => next())
    .catch(err => next(err))
}

module.exports = auth
