const express = require('express');
const homeRoute = require('./home.route');
const userRoute = require('./user.route');
const authRoute = require('./auth.route');
const instrumentsRoute = require('./instruments.route');
const router = express.Router();

const defaultRoutes = [
  {
    path: '/',
    route: homeRoute,
  },
  {
    path: '/user',
    route: userRoute,
  },
  {
    path: '/auth',
    route: authRoute,
  },
  {
    path: '/instruments',
    route: instrumentsRoute,
  }
];

defaultRoutes.forEach((route) => {
  router.use(route.path, route.route);
});

module.exports = router;