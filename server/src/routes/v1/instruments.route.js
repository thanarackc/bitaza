const express = require('express')
const intrumentsController = require('../../controllers/instruments.controlle')
const auth = require('../../middleware/auth')
const router = express.Router()

router.route('/list').post(intrumentsController.findAll)
router.route('/update').post(auth(), intrumentsController.update)

module.exports = router
