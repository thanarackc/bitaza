// import Link from 'next/link'
import React, {
  SyntheticEvent,
  useCallback,
  useContext,
  useEffect
} from 'react'
import { useRouter } from 'next/router'
import { useForm } from 'react-hook-form'
import parseJwt from '../utils/parseJwt'
import { AppContext } from './Layout'

type Props = {}

interface IFormInput {
  username: string
  password: string
}

const Signup: React.FunctionComponent<Props> = () => {
  const { appState, appAction }: any = useContext(AppContext)

  const router = useRouter()

  const {
    register,
    handleSubmit,
    watch,
    errors,
    setError,
    clearErrors
  } = useForm<IFormInput>()

  useEffect(() => {
    // Auto redirect to dashboard
    if (appState.user.isAuth) {
      router.push('/dashboard')
      return
    }
  }, [])

  const submit = useCallback(async data => {
    try {
      const res = await fetch('/api/v1/auth/login', {
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data),
        method: 'post'
      })
      const fetchData = await res.json()
      if (fetchData?.data) {
        const accessToken = fetchData?.data?.accessToken
        window.localStorage.setItem('accessToken', accessToken)
        // Decode token get role
        const decodedToken = parseJwt(accessToken)
        appAction.onSetStaeUser({
          isAuth: true,
          role: decodedToken.sub.role
        })
        router.push('dashboard')
      } else {
        setError('username', {
          type: 'validate',
          message: 'User not found.'
        })
      }
    } catch (e) {
      console.log(e)
    }
  }, [])

  return (
    <section className="flex py-12 items-start justify-center max-w-md m-auto">
      <div className="flex-1 px-2 py-2 sm:px-0 sm:py-0">
        <div className="h-auto bg-gray-100 px-6 py-6 rounded-md">
          <h2 className="text-3xl text-center mb-2">Login</h2>
          <div className="mt-4">
            <form onSubmit={handleSubmit(submit)}>
              <label className="block">
                {errors.username && (
                  <div className="text-red-500 mb-2">
                    {!errors.username.message && (
                      <span>Please Enter Username</span>
                    )}
                    {errors.username.message && (
                      <span>{errors.username.message}</span>
                    )}
                  </div>
                )}
                <input
                  name="username"
                  type="text"
                  className="w-full block form-input"
                  placeholder="Username"
                  maxLength={100}
                  ref={register({ required: true })}
                />
              </label>
              <label className="block mt-4">
                {errors.password && (
                  <div className="text-red-500 mb-2">
                    {!errors.password.message && (
                      <span>กรุณากรอก Password</span>
                    )}
                    {errors.password.message && (
                      <span>{errors.password.message}</span>
                    )}
                  </div>
                )}
                <input
                  name="password"
                  type="password"
                  className="w-full block form-input"
                  placeholder="Password"
                  maxLength={100}
                  ref={register({ required: true })}
                />
              </label>
              <button className="register-button mt-4">Signin</button>
            </form>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Signup
