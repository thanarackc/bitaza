import React, {
  SyntheticEvent,
  useCallback,
  useContext,
  useEffect,
  useState
} from 'react'
import moment from 'moment'
import classes from 'react-style-classes'
import { formatMoney } from '../utils/formatMoney'

type Props = {}

const listLevelItem: any[] = []
let globalWebsocket: WebSocket

const Trade: React.FunctionComponent<Props> = () => {
  const [instruments, setInstruments] = useState([])
  const [subScribeLevel1Item, setSubScribeLevel1Item]: any = useState([])

  const socketOnOpen = (env: any) => {
    console.log('Connected.')
    // Get data after connection.
    if (!instruments.length) {
      const getInstrumentsMessage = JSON.stringify({
        m: 0,
        i: 0,
        n: 'GetInstruments',
        o: JSON.stringify({
          OMSId: 1
        })
      })
      socketPushMessage(getInstrumentsMessage)
    }
  }

  const socketOnMessage = (env: any) => {
    const data = JSON.parse(env.data)
    if (data && data.m === 1 && data.n === 'GetInstruments') {
      const payload = JSON.parse(data.o)
      onGetInstrumentsApi(payload)
    }
    if (data && data.m === 3 && data.n === 'Level1UpdateEvent') {
      const payload = JSON.parse(data.o)
      const exitData = listLevelItem.findIndex(
        (val: any) => val.InstrumentId === payload.InstrumentId
      )
      if (exitData === -1) {
        listLevelItem.push({ ...payload, oldData: payload })
      } else {
        const oldData = listLevelItem[exitData]
        listLevelItem[exitData] = { ...payload, oldData }
      }
      setSubScribeLevel1Item([...listLevelItem])
    }
  }

  const socketPushMessage = (message: string) => {
    globalWebsocket.send(message)
  }

  const onGetInstrumentsApi = async (payload: any) => {
    try {
      const res = await fetch('/api/v1/instruments/list', {
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
          'Content-Type': 'application/json'
        },
        method: 'post'
      })
      const fetchData = await res.json()
      if (fetchData?.data?.result) {
        let newInstrument: any = payload.map((val: any) => {
          const result = fetchData?.data?.result.find(
            (val2: any) => +val.InstrumentId === +val2.instrumentId
          )
          return {
            ...val,
            status: (result && result.status) || ''
          }
        })
        newInstrument = newInstrument.filter(
          (val: any) => val.status === 'active'
        )
        setInstruments(newInstrument)
        // Subscript data
        if (newInstrument.length) {
          newInstrument.forEach((val: any) => {
            const getSubScribeLevel1Item = JSON.stringify({
              m: 0,
              i: 0,
              n: 'SubscribeLevel1',
              o: JSON.stringify({
                OMSId: 1,
                InstrumentId: val.InstrumentId
              })
            })
            socketPushMessage(getSubScribeLevel1Item)
          })
        }
      }
    } catch (e) {
      console.log(e)
    }
  }

  useEffect(() => {
    // Websocket get data
    if (process.browser) {
      const websocket = new WebSocket('wss://apexapi.bitazza.com/WSGateway/')
      websocket.onopen = socketOnOpen
      websocket.onmessage = socketOnMessage
      globalWebsocket = websocket
    }
    return () => {
      setInstruments([])
      setSubScribeLevel1Item([])
    }
  }, [])

  useEffect(() => {
    // console.log(subScribeLevel1Item)
  }, [subScribeLevel1Item])

  const subscribeValue = (instrumentId: number) => {
    const value = subScribeLevel1Item.find(
      (val: any) => val.InstrumentId === instrumentId
    )
    return value
  }

  return (
    <React.Fragment>
      {true && (
        <section className="flex py-12 items-start justify-center max-w-full m-auto">
          <div className="flex-1 px-2 py-2 sm:px-0 sm:py-0">
            <div className="h-auto bg-gray-100 px-6 py-6 rounded-md">
              <h2 className="text-xl font-bold flex flex-row items-center">
                <svg
                  width="24"
                  height="24"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M13 7h8m0 0v8m0-8l-8 8-4-4-6 6"
                  />
                </svg>
                <span className="ml-2">Trade Market</span>
              </h2>
              <div className="grid grid-cols-1">
                <div className="col-span-1">
                  <ul className="mt-4">
                    <li className="hidden sm:block">
                      <div className="flex flex-row border-b border-gray-300 pb-2 mb-2">
                        <span className="w-2/12 text-gray-700 font-bold">
                          Instrument
                        </span>
                        <span className="w-2/12 text-gray-700 font-bold">
                          BestBid
                        </span>
                        <span className="w-2/12 text-gray-700 font-bold">
                          BestOffer
                        </span>
                        <span className="w-2/12 text-gray-700 font-bold">
                          Volume
                        </span>
                        <span className="w-2/12 text-gray-700 font-bold">
                          LastTradedPx
                        </span>
                        <span className="w-2/12 text-gray-700 font-bold">
                          LastTradedQty
                        </span>
                        <span className="w-3/12 text-gray-700 font-bold">
                          TimeStamp
                        </span>
                      </div>
                    </li>
                    {instruments.map((val: any, index: number) => {
                      return (
                        <li key={index} className="mb-4 sm:mb-2">
                          <div className="sm:flex sm:flex-row">
                            <div className="w-full flex justify-start items-center sm:block sm:w-2/12 text-blue-600 font-bold">
                              <svg
                                width="16"
                                height="16"
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                stroke="currentColor"
                                className="mr-2 sm:hidden"
                              >
                                <path
                                  strokeLinecap="round"
                                  strokeLinejoin="round"
                                  strokeWidth={2}
                                  d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z"
                                />
                              </svg>
                              <div>{val.Symbol}</div>
                            </div>
                            <div className="w-full flex sm:w-2/12 text-gray-500 font-medium">
                              <div className="w-5/12 text-sm sm:hidden">BestBid</div>
                              <div
                                className={classes(
                                  subscribeValue(val.InstrumentId)?.BestBid >=
                                    subscribeValue(val.InstrumentId)?.oldData
                                      ?.BestBid
                                    ? 'text-green-700'
                                    : 'text-red-600',
                                )}
                              >
                                {formatMoney({
                                  amount: subscribeValue(val.InstrumentId)
                                    ?.BestBid
                                })}
                              </div>
                            </div>
                            <div className="w-full flex sm:w-2/12 text-gray-500 font-medium">
                              <div className="w-5/12 text-sm sm:hidden">BestOffer</div>
                              <div
                                className={classes(
                                  subscribeValue(val.InstrumentId)?.BestOffer >=
                                    subscribeValue(val.InstrumentId)?.oldData
                                      ?.BestOffer
                                    ? 'text-green-700'
                                    : 'text-red-600'
                                )}
                              >
                                {formatMoney({
                                  amount: subscribeValue(val.InstrumentId)
                                    ?.BestOffer
                                })}
                              </div>
                            </div>
                            <div className="w-full flex sm:w-2/12 text-gray-500 font-medium">
                              <div className="w-5/12 text-sm sm:hidden">Volume</div>
                              <div
                                className={classes(
                                  subscribeValue(val.InstrumentId)?.Volume >=
                                    subscribeValue(val.InstrumentId)?.oldData
                                      ?.Volume
                                    ? 'text-green-700'
                                    : 'text-red-600'
                                )}
                              >
                                {subscribeValue(val.InstrumentId)?.Volume ||
                                  '-'}
                              </div>
                            </div>
                            <div className="w-full flex sm:w-2/12 text-gray-500 font-medium">
                              <div className="w-5/12 text-sm sm:hidden">
                                LastTradedPx
                              </div>
                              <div
                                className={classes(
                                  subscribeValue(val.InstrumentId)
                                    ?.LastTradedPx >=
                                    subscribeValue(val.InstrumentId)?.oldData
                                      ?.LastTradedPx
                                    ? 'text-green-700'
                                    : 'text-red-600'
                                )}
                              >
                                {formatMoney({
                                  amount: subscribeValue(val.InstrumentId)
                                    ?.LastTradedPx
                                })}
                              </div>
                            </div>
                            <div className="w-full flex sm:w-2/12 text-gray-500 font-medium">
                              <div className="w-5/12 text-sm sm:hidden">
                                LastTradedQty
                              </div>
                              <div
                                className={classes(
                                  subscribeValue(val.InstrumentId)
                                    ?.LastTradedQty >=
                                    subscribeValue(val.InstrumentId)?.oldData
                                      ?.LastTradedQty
                                    ? 'text-green-700'
                                    : 'text-red-600'
                                )}
                              >
                                {subscribeValue(val.InstrumentId)
                                  ?.LastTradedQty || '-'}
                              </div>
                            </div>
                            <div className="w-full flex sm:w-3/12 text-gray-500 font-medium">
                              <div className="w-5/12 text-sm sm:hidden">TimeStamp</div>
                              {subscribeValue(val.InstrumentId)?.TimeStamp
                                ? moment(
                                    +subscribeValue(val.InstrumentId)?.TimeStamp
                                  ).format('YYYY MMM DD hh:mm')
                                : '-'}
                            </div>
                          </div>
                        </li>
                      )
                    })}
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </section>
      )}
    </React.Fragment>
  )
}

export default Trade
