import { useRouter } from 'next/router'
import React, {
  useCallback,
  useContext,
  useEffect,
  useState
} from 'react'
import { AppContext } from './Layout'
import classes from 'react-style-classes'
import { useForm } from 'react-hook-form'

type Props = {}
let globalWebsocket: WebSocket

const Dashboard: React.FunctionComponent<Props> = () => {
  const router = useRouter()
  const { handleSubmit, register, watch, setValue } = useForm({
    defaultValues: {
      status: '',
      instrumentIds: []
    }
  })
  const watchStatus = watch('status')
  const watchInstrumentIds = watch('instrumentIds')
  const [productItems, setProductItem] = useState([])
  const [instruments, setInstruments] = useState([])
  const [tab, setTab] = useState(1)
  const { appState }: any = useContext(AppContext)

  const socketOnOpen = (env: any) => {
    console.log('Connected.')
    // Get data after connection.
    if (!productItems.length) {
      const getProductMessage = JSON.stringify({
        m: 0,
        i: 0,
        n: 'GetProducts',
        o: JSON.stringify({
          OMSId: 1
        })
      })
      socketPushMessage(getProductMessage)
    }
    if (!instruments.length) {
      const getInstrumentsMessage = JSON.stringify({
        m: 0,
        i: 0,
        n: 'GetInstruments',
        o: JSON.stringify({
          OMSId: 1
        })
      })
      socketPushMessage(getInstrumentsMessage)
    }
  }

  const socketOnMessage = (env: any) => {
    const data = JSON.parse(env.data)
    // Handle get price data
    if (data && data.m === 1 && data.n === 'GetProducts') {
      const payload = JSON.parse(data.o).filter((v: any) => !v.IsDisabled)
      setProductItem(payload)
    }
    if (data && data.m === 1 && data.n === 'GetInstruments') {
      const payload = JSON.parse(data.o)
      onGetInstrumentsApi(payload)
    }
  }

  const socketPushMessage = (message: string) => {
    globalWebsocket.send(message)
  }

  useEffect(() => {
    // Check Role
    if (!appState.user.isAuth) {
      router.push('/signup')
      return
    }
    // Websocket get data
    if (process.browser) {
      const websocket = new WebSocket('wss://apexapi.bitazza.com/WSGateway/')
      websocket.onopen = socketOnOpen
      websocket.onmessage = socketOnMessage
      globalWebsocket = websocket
    }
    return () => {
      setProductItem([])
      setInstruments([])
    }
  }, [])

  const onSubmitUpdateInstruments = useCallback(
    async (data: any) => {
      try {
        const res = await fetch('/api/v1/instruments/update', {
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(data),
          method: 'post'
        })
        const fetchData = await res.json()
        if (fetchData?.data) {
          await onGetInstrumentsApi(instruments)
          setValue('instrumentIds', [])
          alert('Saved.')
        }
      } catch (e) {
        console.log(e)
      }
    },
    [instruments]
  )

  const onGetInstrumentsApi = async (payload: any) => {
    try {
      const res = await fetch('/api/v1/instruments/list', {
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
          'Content-Type': 'application/json'
        },
        method: 'post'
      })
      const fetchData = await res.json()
      if (fetchData?.data?.result) {
        const newInstrument: any = payload.map((val: any) => {
          const result = fetchData?.data?.result.find(
            (val2: any) => +val.InstrumentId === +val2.instrumentId
          )
          return {
            ...val,
            status: (result && result.status) || 'inactive'
          }
        })
        setInstruments(newInstrument)
      }
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <React.Fragment>
      {true && (
        <section className="flex py-12 items-start justify-center max-w-full m-auto">
          <div className="flex-1">
            <div className="h-auto bg-gray-100 px-6 py-6 rounded-md">
              <h2 className="text-xl font-bold flex flex-row items-center">
                <svg
                  width="24"
                  height="24"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"
                  />
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                  />
                </svg>
                <span className="ml-2">Trade Market</span>
              </h2>
              <div className="tabs flex flex-row justify-center sm:justify-start border-b-2 border-green-600 mt-4">
                <div
                  role="button"
                  onClick={() => setTab(1)}
                  className={classes(
                    'px-4 py-2',
                    'cursor-pointer',
                    tab === 1 && 'text-white bg-green-600',
                    tab !== 1 && 'text-white bg-gray-600'
                  )}
                >
                  Products
                </div>
                <div
                  onClick={() => setTab(2)}
                  className={classes(
                    'px-4 py-2',
                    'cursor-pointer',
                    tab === 2 && 'text-white bg-green-600',
                    tab !== 2 && 'text-white bg-gray-600'
                  )}
                >
                  Instruments
                </div>
              </div>
              {tab === 1 && (
                <div id="data-tab-1" className="mt-4">
                  <ul>
                    <li>
                      <div className="flex flex-row border-b border-gray-300 pb-2 mb-2">
                        <span className="text-gray-700 font-bold w-10 text-right mr-6"></span>
                        <span className="w-5/12 sm:2/12 text-gray-700 font-bold">
                          Coin Name
                        </span>
                        <span className="flex-1 text-gray-500">
                          Coin Full Name
                        </span>
                      </div>
                    </li>
                    {productItems.map((val: any, index: number) => {
                      return (
                        <li key={index} className="mb-2">
                          <div className="flex flex-row">
                            <span className="text-gray-700 font-bold w-10 text-right mr-6">
                              #{index + 1}
                            </span>
                            <span className="w-5/12 sm:2/12 text-gray-700 font-bold">
                              {val.Product}
                            </span>
                            <span className="flex-1 text-gray-500">
                              {val.ProductFullName}
                            </span>
                          </div>
                        </li>
                      )
                    })}
                  </ul>
                </div>
              )}
              {tab === 2 && (
                <div id="data-tab-1" className="mt-4">
                  <form onSubmit={handleSubmit(onSubmitUpdateInstruments)}>
                    <div
                      id="action-zone"
                      className="flex flex-row items-center justify-center sm:justify-items-start"
                    >
                      <select
                        ref={register}
                        name="status"
                        className="w-6/12 sm:w-2/12 border bg-white rounded px-2 py-2 outline-none ml-2"
                        defaultValue=""
                      >
                        <option value="">-- Select Action --</option>
                        <option value="active">Active</option>
                        <option value="inactive">InActive</option>
                      </select>
                      <button
                        disabled={
                          watchStatus === '' || watchInstrumentIds.length === 0
                        }
                        className="ml-2 px-4 py-1 outline-none border-none bg-gray-800 rounded-sm text-white disabled:opacity-50"
                      >
                        Save
                      </button>
                    </div>
                    <ul className="mt-4">
                      <li>
                        <div className="flex flex-row border-b border-gray-300 pb-2 mb-2">
                          <span className="text-gray-700 font-bold w-10 text-right mr-6"></span>
                          <span className="w-5/12 sm:2/12 text-gray-700 font-bold">
                            Instrument
                          </span>
                          <span className="flex-1 text-gray-500">Status</span>
                        </div>
                      </li>
                      {instruments.map((val: any, index: number) => {
                        return (
                          <li key={index} className="mb-2">
                            <div className="flex flex-row">
                              <span className="text-gray-700 font-bold w-10 text-right mr-6">
                                <input
                                  ref={register}
                                  type="checkbox"
                                  name="instrumentIds"
                                  value={val.InstrumentId}
                                  defaultChecked={val.InstrumentId}
                                />
                              </span>
                              <span className="w-5/12 sm:2/12 text-gray-700 font-bold">
                                {val.Symbol}
                              </span>
                              <span className="flex-1 text-gray-500">
                                {val.status === 'active' && (
                                  <span className="text-green-600">
                                    Available
                                  </span>
                                )}
                                {val.status === 'inactive' && (
                                  <span className="text-red-600">
                                    No Available
                                  </span>
                                )}
                              </span>
                            </div>
                          </li>
                        )
                      })}
                    </ul>
                  </form>
                </div>
              )}
            </div>
          </div>
        </section>
      )}
    </React.Fragment>
  )
}

export default Dashboard
