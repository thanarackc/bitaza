import { useRouter } from 'next/router'
import React, { useContext, useEffect } from 'react'
import { AppContext } from './Layout'

type Props = {}

const Logout: React.FunctionComponent<Props> = () => {
  const router = useRouter()
  const { appState, appAction }: any = useContext(AppContext)

  useEffect(() => {
    if (appState.user.isAuth) {
      appAction.onSetStaeUser({
        isAuth: false,
        role: '',
        user: ''
      })
      window.localStorage.removeItem('accessToken')
      router.push('/')
      return
    }
  }, [])

  return <div></div>
}

export default Logout
