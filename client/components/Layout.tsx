import React, { ReactNode, useState } from 'react'
import Link from 'next/link'
import Head from 'next/head'

type Props = {
  children?: ReactNode
  title?: string
}

type InitialState = {
  appState: any
  appAction: any
}

export const AppContext = React.createContext({ appState: '', appAction: '' })

const initialValue = {
  user: {
    isAuth: false,
    role: '',
    name: ''
  }
}

const Layout = ({ children, title = 'This is the default title' }: Props) => {
  const [appState, setAppState] = useState(initialValue)

  const onSetStaeUser = (state: any) => {
    setAppState(prevState => ({
      ...prevState,
      user: state
    }))
  }

  const appStore: InitialState | any = {
    appState,
    appAction: {
      onSetStaeUser
    }
  }

  return (
    <div>
      <Head>
        <title>{title}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <div className="bg-gray-800 min-h-screen w-auto">
        <div className="container md:mx-auto h-full">
          <div className="flex flex-col sm:flex-row items-center">
            <div className="px-4 py-8 justify-center sm:justify-start sm:px-0 w-full flex sm:py-8">
              <Link href="/">
                <a>
                  <img src="/images/logo.png" width="200" />
                </a>
              </Link>
            </div>
            <div className="flex justify-center sm:justify-end w-full">
              <ul className="p-0 m-0 list-none text-white flex">
                <li>
                  <Link href="/">Home</Link>
                </li>
                <li className="ml-3">
                  <Link href="/trade">Trade</Link>
                </li>
                {appState.user.isAuth && (
                  <li className="ml-3">
                    <Link href="/dashboard">Dashboard</Link>
                  </li>
                )}
                <li className="ml-3">
                  {!appState.user.isAuth && <Link href="/signup">Login</Link>}
                  {appState.user.isAuth && <Link href="/logout">Sign Out</Link>}
                </li>
              </ul>
            </div>
          </div>
          <AppContext.Provider value={appStore}>{children}</AppContext.Provider>
        </div>
      </div>
    </div>
  )
}

export default Layout
