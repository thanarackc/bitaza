import Link from 'next/link'
import React from 'react'

type Props = {}

const Home: React.FunctionComponent<Props> = () => {
  return (
    <section className="flex py-12 items-start justify-center max-w-md m-auto">
      <div className="flex-1 px-2 py-2 sm:px-0 sm:py-0">
        <div className="h-auto bg-gray-100 px-6 py-6 rounded-md">
          <h2 className="text-3xl text-center mb-2 text-green-800">
            Welcome To Bitazza
          </h2>
          <h4 className="text-center text-gray-500">
            Discover the next-level digital asset trading experience on a
            regulated platform.
          </h4>
          <div className="block mt-6 text-center">
            <Link href="/trade">
              <a className="login-button">Start !</a>
            </Link>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Home
