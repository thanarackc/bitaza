import React, { ComponentClass } from 'react'
import Layout from '../../components/Layout'
import '../../styles/globals.scss'

type Props = {
  Component: ComponentClass<any>
  pageProps: any
}

const MyApp: React.FunctionComponent<Props> = ({ Component, pageProps }) => {
  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>
  )
}

export default MyApp
