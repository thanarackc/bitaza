const bundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: !!process.env.BUNDLE_ANALYZE
})

module.exports = bundleAnalyzer({
  typescript: {
    ignoreBuildErrors: true
  },
  i18n: {
    locales: ['en-US', 'es'],
    defaultLocale: 'en-US'
  },
  async rewrites() {
    const ENDPOINT =
      process.env.NODE_ENV === 'development'
        ? process.env.API_DOMAIN_LOCAL
        : process.env.API_DOMAIN_PRODUCTION
    return [
      {
        source: '/api/:path*',
        destination: ENDPOINT + '/:path*',
        basePath: false
      }
    ]
  }
})
